import { StyleSheet,Dimensions } from "react-native";

const {height,width} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
    menuContainer: {
        marginVertical: height-(height-20),
        marginHorizontal: width-(width-20), 
        justifyContent: 'center', 
        alignItems: 'center',
        elevation: 4,
        padding: 15,
        backgroundColor: '#fff',
        borderRadius: 4,
    }
})

export default styles;