
import { StyleSheet, Dimensions } from "react-native";

const { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
    containerItemGreen: {
        padding: 20,
        margin: 5,
        borderRadius: 5,
        backgroundColor: 'green',
        elevation: 3
    },
    containerItemYellow: {
        padding: 20,
        margin: 5,
        borderRadius: 5,
        backgroundColor: 'yellow',
        elevation: 3
    },
    containerItemRed: {
        padding: 20,
        margin: 5,
        borderRadius: 5,
        backgroundColor: 'red',
        elevation: 3
    }
})

export default styles;