import fs from 'react-native-fs';

export const getIconSource = (value) => {
    switch (value) {
        case 'icon-survey':
            return require('../assets/icon_survey.png');
            break;
        default:
            return require('../assets/icon_survey.png');
            break;
    }
}

export const writeFile = async (oldFileName = undefined, newFileName, data) => {
    const loadExistingFile = async (oldFileName) => {
        try {
            let pathExtName = `${fs.DocumentDirectoryPath}/${oldFileName}`
            let load = await fs.readFile(pathExtName, 'utf8').then(content => content)
            console.log('load', load);

            return load;
        } catch (error) {
            return false;
        }
    }

    const deleteExistingFile = async (oldFileName) => {
        try {
            let pathExtName = `${fs.DocumentDirectoryPath}/${oldFileName}`
            let del = await fs.unlink(pathExtName);
            return true
        } catch (error) {
            return false;
        }
    }

    try {
        let file = newFileName + '.jpg'
        let path = `${fs.DocumentDirectoryPath}/${file}`
        console.log({ filename: file, path: path });

        const resExistFile = await loadExistingFile(oldFileName);
        if (resExistFile === false) {
            const resp = await fs.writeFile(path, data, 'base64');
        } else {
            const del = await deleteExistingFile(oldFileName)
            const resp = await fs.writeFile(path, data, 'base64');
        }

        return file
    } catch (error) {
        console.log('error message', error);

        return false;
    }
}
/**
 * @param {Any} fileName (get from reducer)
 * Load File From External Storage
 */
export const loadFile = async (fileName) => {
    try {
        let path = `${fs.DocumentDirectoryPath}/${fileName}`

        let load = await fs.readFile(path, 'base64').then(content => content)
        console.log({ path: path, load: load });
        return load;
    } catch (error) {
        return false;
    }
}

/**
 * Delete File from External Storage
 * @param {String} fileName (filename from reducer)
 */
export const deleteFile = async (fileName) => {
    try {
        let path = `${fs.DocumentDirectoryPath}/${fileName}`
        console.log('deleteFile path', path)
        let del = await fs.unlink(path);
        let lastDir = `${fs.DocumentDirectoryPath}`
        console.log('deleteFile lastDir', lastDir)
        return true
    } catch (error) {
        return false;
    }
}

export const readRootDir = async () => {
    try {
        const path = `${fs.DocumentDirectoryPath}`;
        const res = await fs.readDir(path).then(content => content);
        return res
    } catch (error) {
        __DEV__ && console.log(error);
    }
}