import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import MainScreen from "../contents/screens/MainScreen";
import Survey1 from "../contents/screens/Survey1";
import Survey2 from "../contents/screens/Survey2";
import Survey3 from "../contents/screens/Survey3";
import DetailScreen from "../contents/screens/DetailScreen";
import DetailScreen2 from '../contents/screens/DetailScreen2'
import ListTiangScreen from "../contents/screens/ListTiangScreen";
import AddTiangScreen from "../contents/screens/AddTiangScreen";
import DetailTiangScreen from "../contents/screens/DetailTiangScreen";
import Surveyor from "../contents/screens/Surveyor";
import DetailSurveyor from "../contents/screens/DetailSurveyor";

const RouteConfig = {
    Main: {
        screen: MainScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle: 'Home Survey',
            headerStyle: {
                backgroundColor: '#304ffe',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        })
    },
    Survey1: {
        screen: Survey1
    },
    Survey2: {
        screen: Survey2
    },
    Survey3: {
        screen: Survey3
    },
    Surveyor: {
        screen: Surveyor,
        navigationOptions: ({ navigation }) => ({
            headerTitle: 'List Surveyor',
            headerStyle: {
                backgroundColor: '#304ffe',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        })
    },
    DetailScreen: {
        screen: DetailScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle: 'Isi Data',
            headerStyle: {
                backgroundColor: '#304ffe',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        })
    },
    DetailScreen2: {
        screen: DetailScreen2,
        navigationOptions: ({ navigation }) => ({
            headerTitle: 'Isi Data',
            headerStyle: {
                backgroundColor: '#304ffe',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        })
    },
    ListTiang: {
        screen: ListTiangScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle: 'Tambahkan Tiang',
            headerStyle: {
                backgroundColor: '#304ffe',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        })
    },
    AddTiang: {
        screen: AddTiangScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle: 'Tambahkan Tiang',
            headerStyle: {
                backgroundColor: '#304ffe',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        })
    },
    DetailTiang: {
        screen: DetailTiangScreen,
        navigationOptions: ({ navigation }) => ({
            headerTitle: 'Tambahkan Tiang',
            headerStyle: {
                backgroundColor: '#304ffe',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }),
    },
    DetailSurveyor: {
        screen: DetailSurveyor,
        navigationOptions: ({ navigation }) => ({
            headerTitle: navigation.state.params.data == null ? 'Tambahkan Surveyor' : 'Update Surveyor',
            headerStyle: {
                backgroundColor: '#304ffe',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }),
    }
}

const StackNavigator = createStackNavigator(RouteConfig)

export default createAppContainer(StackNavigator);