import req from './Caller'
import { BASE_URL } from '../constants/Env'

const getWilayah = async () => {
    let resp = await req.appGet(`${BASE_URL}/getwilayah`)
    return resp;
}

// Update Survey
const updateSurvey = async (data) => {
    let resp = await req.appPost(`${BASE_URL}/updateSurvey`, data)
    return resp;
}

// Add Survey 1.1
const saveSurvey = async (data) => {
    let resp = await req.appPost(`${BASE_URL}/savenewsurvey`, data)
    return resp
}

// Survey 1.2 Update
const saveSurvey2 = async (data) => {
    let resp = await req.appPost(`${BASE_URL}/savesurvey2`, data)
    return resp;
}

const getAllSurvey = async () => {
    let resp = await req.appGet(`${BASE_URL}/getAllSurvey`)
    return resp
}

const getSurvey = async (data) => {
    let resp = await req.appPost(`${BASE_URL}/getdatasurvey`, data)
    return resp
}

const getTiang = async (data) => {
    let resp = await req.appPost(`${BASE_URL}/gettiang`, data)
    return resp
}

const addTiang = async (data) => {
    let resp = await req.appPost(`${BASE_URL}/addtiang`, data)
    return resp
}

const updateTiang = async (data) => {
    let resp = await req.appPost(`${BASE_URL}/edittiang`, data)
    return resp
}

const getAllSurveyor = async () => {
    let resp = await req.appPost(`${BASE_URL}/getAllSurveyor`);
    return resp;
}

const addSurveyor = async (data) => {
    let resp = await req.appPost(`${BASE_URL}/addSurveyor`, data);
    return resp;
}

const updateSurveyor = async (data) => {
    let resp = await req.appPost(`${BASE_URL}/updateSurveyor`, data);
    return resp;
}


export default { getWilayah, updateSurvey, saveSurvey, saveSurvey2, getAllSurvey, getSurvey, getTiang, addTiang, updateTiang, getAllSurveyor, addSurveyor, updateSurveyor }