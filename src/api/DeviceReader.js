import DeviceInfo from 'react-native-device-info';

export default readDevice = async () => {
    const params = {
        applicationName:DeviceInfo.getApplicationName(),
        batteryLevel: await DeviceInfo.getBatteryLevel(),
        brand:DeviceInfo.getBrand(),
        buildNumber:DeviceInfo.getBuildNumber(),
        bundleId:DeviceInfo.getBundleId(),
        carrier:DeviceInfo.getCarrier(),
        deviceId:DeviceInfo.getDeviceId(),
        deviceName:DeviceInfo.getDeviceName(),
        macAddr:await DeviceInfo.getMacAddress(),
        manufacturer:DeviceInfo.getManufacturer(),
        model:DeviceInfo.getModel(),
        phoneNumber: await DeviceInfo.getPhoneNumber(),
        buildId:DeviceInfo.getBuildId(),
        uniqueId:DeviceInfo.getUniqueId(),
        userAgent:DeviceInfo.getUserAgent(),
        version:DeviceInfo.getVersion(),   
        isTablet:DeviceInfo.isTablet(),
        isLandscape:DeviceInfo.isLandscape(),
        deviceType:DeviceInfo.getDeviceType(),
        isLocationEnable:await DeviceInfo.isLocationEnabled(),
        availLocationProvs:await DeviceInfo.getAvailableLocationProviders(),
        systemAvailFeature:await DeviceInfo.getSystemAvailableFeatures(),
        systemName:DeviceInfo.getSystemName(),
        systemVersion:DeviceInfo.getSystemVersion(),
        readableVersion:DeviceInfo.getReadableVersion()
    }   
    return params;
}