export const menu = [
    {
        id: 0,
        name: 'Survey 1',
        route: 'Survey1',
        image: 'icon-survey'
    },
    {
        id: 1,
        name: 'Survey 1.2',
        route: 'Survey2',
        image: 'icon-survey'
    },
    {
        id: 2,
        name: 'Tiang dan Lampu',
        route: 'Survey3',
        image: 'icon-survey'
    },
    {
        id: 3,
        name: 'Surveyor',
        route: 'Surveyor',
        image: 'icon-survey'
    }
]