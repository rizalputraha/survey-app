export const ketPicker = [
    { label: 'Berfungsi', value: 'Berfungsi' },
    { label: 'Rusak', value: 'Rusak' },
    { label: 'Tidak Ada', value: 'Tidak Ada' },
]

export const switchPicker = [
    { label: 'Timer', value: 'Timer' },
    { label: 'Photocell', value: 'Photocell' },
    { label: 'Manual', value: 'Manual' },
]

export const kabelPicker = [
    { label: 'Twisted', value: 'Twisted' },
    { label: 'Untwisted', value: 'Untwisted' },
]

export const groundingPicker = [
    { label: 'Ada', value: 'Ada' },
    { label: 'Tidak Ada', value: 'Tidak Ada' },
]

export const jenisTiangPicker = [
    { label: 'PLN', value: 'PLN' },
    { label: 'Swadaya', value: 'Swadaya' },
]

export const jenisKabelPicker = [
    { label: 'Twisted', value: 'Twisted' },
    { label: 'Untwisted', value: 'Untwisted' },
]

export const jenisLampuPicker = [
    { label: 'LED', value: 'LED' },
    { label: 'LHE', value: 'LHE' },
    { label: 'Pelepas Gas', value: 'Pelepas Gas' },
    { label: 'TL', value: 'TL' },
    { label: 'Lain-Lain', value: 'Lain-Lain' },
]