export const wilayah = [
    {
        id: 0,
        name: 'Surabaya'
    },
    {
        id: 1,
        name: 'Tangerang'
    },
    {
        id: 2,
        name: 'Jakarta'
    },
    {
        id: 3,
        name: 'Yogyakarta'
    },
    {
        id: 4,
        name: 'Sidoarjo'
    },
]