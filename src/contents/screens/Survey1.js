import React, { Component } from 'react'
import { Text, View, TouchableOpacity, FlatList, ActivityIndicator, Alert } from 'react-native'
import { Input, Icon, Button } from "react-native-elements";
import SelectWilayah from '../components/SelectWilayah';
import InputComponent from '../components/InputComponent';
import { pelanggan } from "../../constants/pelanggan";
import { wilayah } from "../../constants/wilayah";
import Survey from "../../api/Survey";
import styles from "../../styles/Survey1Styles";
import ActionButton from 'react-native-action-button';

export class Survey1 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            wilayah: '',
            dataWilayah: null,
            data: null,
            searchWilayah: '',
            searchKey: '',
        }
    }

    static navigationOptions = ({ navigation }) => ({
        headerTitle: navigation.state.params.screen
    })

    async componentDidMount() {
        await this.setState({ loading: true });

        let wilayah = await Survey.getWilayah().then(data => data).catch(err => err);
        let dataSurvey = await Survey.getAllSurvey().then(data => data).catch(err => err);

        await this.setState({
            dataWilayah: wilayah.Data,
            data: dataSurvey.Data,
            loading: false,
        })
        await this.setState({ loading: false });

    }

    async onPressSearch() {
        if (this.state.wilayah == '') {
            Alert.alert(
                'Error',
                'Silahkan isi data wilayah',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
            );
        } else {
            await this.setState({ loading: true });
            const formData = new FormData();
            if (this.state.searchKey == '') {
                formData.append('Wilayah', this.state.wilayah);
                let dataSurvey = await Survey.getSurvey(formData);
                await this.setState({
                    loading: false,
                    data: dataSurvey.Data
                })
            } else {
                formData.append('Wilayah', this.state.wilayah);
                formData.append('Id_Pel', parseInt(this.state.searchKey));
                let dataSurvey = await Survey.getSurvey(formData);
                console.log(dataSurvey);
                await this.setState({
                    loading: false,
                    data: dataSurvey.Data
                })
            }
        }
    }

    async onPressReset() {
        await this.setState({ loading: true });

        let wilayah = await Survey.getWilayah();
        let dataSurvey = await Survey.getAllSurvey();

        await this.setState({
            dataWilayah: wilayah.Data,
            data: dataSurvey.Data,
            searchKey: '',
            wilayah: '',
            loading: false,
        })
        await this.setState({ loading: false });
    }

    async _changeState(value, parentState) {
        await this.setState({ [parentState]: value });
    }

    renderItem(item) {
        if (item.Tanggal1 != null) {
            if (item.Tanggal2 != null) {
                return (
                    <TouchableOpacity
                        style={styles.containerItemYellow}
                        onPress={() => this.props.navigation.push('DetailScreen', { data: item })}
                    >
                        <Text>{item.NoUrut}</Text>
                        <Text>{item.IDPel}</Text>
                        <Text>{item.Nama}</Text>
                        <Text>{item.Wilayah}</Text>
                    </TouchableOpacity>
                )
            } else {
                return (
                    <TouchableOpacity
                        style={styles.containerItemGreen}
                        onPress={() => this.props.navigation.push('DetailScreen', { data: item })}
                    >
                        <Text>{item.NoUrut}</Text>
                        <Text>{item.IDPel}</Text>
                        <Text>{item.Nama}</Text>
                        <Text>{item.Wilayah}</Text>
                    </TouchableOpacity>
                )
            }
        } else {
            return (
                <TouchableOpacity
                    style={styles.containerItemRed}
                    onPress={() => this.props.navigation.push('DetailScreen', { data: item })}
                >
                    <Text>{item.NoUrut}</Text>
                    <Text>{item.IDPel}</Text>
                    <Text>{item.Nama}</Text>
                    <Text>{item.Wilayah}</Text>
                </TouchableOpacity>
            )
        }
    }

    render() {
        return (
            this.state.loading && <ActivityIndicator size="large" color="#0000ff" /> ||
            <View style={{ padding: 10, flex: 1 }}>
                <SelectWilayah
                    data={this.state.dataWilayah}
                    titleEmptyData='Pilih Wilayah'
                    placeholder='Cari Wilayah'
                    onChange={(item) => this._changeState(item, 'wilayah')} />
                <InputComponent
                    onChangeText={(value) => this.setState({ 'searchKey': value })}
                    value={this.state.searchKey}
                    placeholder='IDPEL'
                    rightIcon={
                        <Icon
                            name='search'
                            color="#2699FB"
                            size={24}
                        />
                    }
                />
                <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                    <Button
                        onPress={() => this.onPressReset()}
                        containerStyle={{ flex: 1 }}
                        buttonStyle={{ padding: 10, marginRight: 5 }}
                        type='outline'
                        title='RESET PENCARIAN'
                        titleStyle={{ fontSize: 13 }}
                    />
                    <Button
                        onPress={() => this.onPressSearch()}
                        containerStyle={{ flex: 1 }}
                        buttonStyle={{ padding: 10, marginLeft: 5 }}
                        type='solid'
                        title='CARI DATA'
                        titleStyle={{ fontSize: 14 }}
                    />
                </View>
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    data={this.state.data}
                    renderItem={({ item }) => this.renderItem(item)}
                />

                <ActionButton onPress={() => this.props.navigation.push('DetailScreen', { data: null })} fixNativeFeedbackRadius={true} shadowStyle={{ elevation: 3 }} buttonColor="#009688" />
            </View>
        )
    }
}

export default Survey1
