import React, { Component } from 'react'
import { Text, View, StatusBar, FlatList, Image, TouchableOpacity, Dimensions, StyleSheet } from 'react-native'
import styles from "../../styles/MainStyles";
import { menu } from "../../constants/menu";
import { getIconSource } from "../../helper/helper";

const { height, width } = Dimensions.get('window');

export class MainScreen extends Component {

    renderMenu(item) {
        return (
            <TouchableOpacity onPress={() => this.props.navigation.push(item.route, { screen: item.name, data: item })} style={styles.menuContainer}>
                <Image source={getIconSource(item.image)} style={{ height: 100, width: 100 }} />
                <Text>{item.name}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar backgroundColor="#0026ca" barStyle="light-content" />
                <View>
                    <Text>Total Data :</Text>
                    <Text>Data yang belum diisi : </Text>
                </View>
                <FlatList
                    data={menu}
                    keyExtractor={item => item.id.toString()}
                    renderItem={({ item }) => this.renderMenu(item)}
                    numColumns={2}
                />

            </View>
        )
    }
}

export default MainScreen
