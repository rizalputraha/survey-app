import React, { Component } from 'react'
import { Text, View, TouchableOpacity, FlatList, ActivityIndicator, Alert, RefreshControl } from 'react-native'
import { Input, Icon, Button } from "react-native-elements";
import SelectWilayah from '../components/SelectWilayah';
import InputComponent from '../components/InputComponent';
import Survey from "../../api/Survey";
import styles from "../../styles/Survey1Styles";

export class ListTiangScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKey: null,
            loading: false,
            data: null,
            dataWilayah: null,
            refreshing: false,
            ...this.props.navigation.state.params.data
        }
    }

    static navigationOptions = ({ navigation }) => ({
        headerTitle: navigation.state.params.screen
    })

    async componentDidMount() {
        const formData = new FormData()
        formData.append('IDPel', this.state.IDPel)

        await this.setState({ loading: true });

        let wilayah = await Survey.getWilayah();
        let dataSurvey = await Survey.getTiang(formData);

        if (dataSurvey.Data == []) {
            await this.setState({
                dataWilayah: wilayah.Data,
                data: null,
                loading: false,
            })
        } else {
            await this.setState({
                dataWilayah: wilayah.Data,
                data: dataSurvey.Data,
                loading: false,
            })
        }
        await this.setState({ loading: false });
    }

    async onPressSearch() {
        if (this.state.wilayah == '') {
            Alert.alert(
                'Error',
                'Silahkan isi data wilayah',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
            );
        } else {
            await this.setState({ loading: true });
            const formData = new FormData();
            if (this.state.searchKey == '') {
                formData.append('Wilayah', this.state.wilayah);
                let dataSurvey = await Survey.getSurvey(formData);
                await this.setState({
                    loading: false,
                    data: dataSurvey.Data
                })
            } else {
                formData.append('Wilayah', this.state.wilayah);
                formData.append('Id_Pel', parseInt(this.state.searchKey));
                let dataSurvey = await Survey.getSurvey(formData);
                await this.setState({
                    loading: false,
                    data: dataSurvey.Data
                })
            }
        }
    }

    async onPressReset() {
        await this.setState({ loading: true });

        let wilayah = await Survey.getWilayah();
        let dataSurvey = await Survey.getAllSurvey();

        await this.setState({
            dataWilayah: wilayah.Data,
            data: dataSurvey.Data,
            searchKey: '',
            wilayah: '',
            loading: false,
        })
        await this.setState({ loading: false });
    }

    async _changeState(value, parentState) {
        await this.setState({ [parentState]: value });
    }

    async _onRefresh() {
        await this.setState({ refreshing: true });
        const formData = new FormData()
        formData.append('IDPel', this.state.IDPel)

        let wilayah = await Survey.getWilayah();
        let dataSurvey = await Survey.getTiang(formData);

        if (dataSurvey.Data == []) {
            await this.setState({
                dataWilayah: wilayah.Data,
                data: null,
                loading: false,
            })
        } else {
            await this.setState({
                dataWilayah: wilayah.Data,
                data: dataSurvey.Data,
                loading: false,
            })
        }
        await this.setState({ refreshing: false });
    }

    renderItem(item) {
        return (
            <TouchableOpacity
                key={item.NoTiang}
                style={styles.containerItem}
                onPress={() => this.props.navigation.push('DetailTiang', { data: item })}
            >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text>{item.Nama}</Text>
                    <Text>{item.NoTiang}</Text>
                </View>
                <Text>{item.IDPel}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            this.state.loading && <ActivityIndicator size="large" color="#0000ff" /> ||
            <View style={{ padding: 10, flex: 1 }}>
                <View style={styles.containerItem}>
                    <Text>No Urut: {this.state.NoUrut}</Text>
                    <Text>IDPel: {this.state.IDPel}</Text>
                    <Text>Nama: {this.state.Nama}</Text>
                    <Text>Wilayah: {this.state.Wilayah}</Text>
                </View>

                <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                    <Button
                        containerStyle={{ flex: 1 }}
                        buttonStyle={{ padding: 10, marginHorizontal: 7 }}
                        type='solid'
                        title='Tambah Tiang'
                        onPress={() => this.props.navigation.push('DetailTiang', { data: { IDPel: this.state.IDPel, Nama: this.state.Nama, NoUrut: null } })}
                        titleStyle={{ fontSize: 14 }}
                    />
                </View>
                {this.state.data == 0 ? <Text>Data Tiang Kosong</Text> :
                    <FlatList
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={() => this._onRefresh()}
                            />
                        }
                        keyExtractor={(item, index) => index.toString()}
                        data={this.state.data}
                        renderItem={({ item }) => this.renderItem(item)}
                    />
                }
            </View>
        )
    }
}

export default ListTiangScreen
