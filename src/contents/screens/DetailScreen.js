import React, { Component } from 'react'
import { Image, Text, View, ScrollView, Alert, Platform, ActivityIndicator, PermissionsAndroid } from 'react-native'
import { Button, Icon } from "react-native-elements";
import InputComponent from "../components/InputComponent";
import DatePicker from "../components/DatePicker";
import Survey from "../../api/Survey";
import RNPickerSelect from 'react-native-picker-select';
import { ketPicker, switchPicker, kabelPicker, groundingPicker } from '../../constants/picker'
import Geolocation from "react-native-geolocation-service";
import ImagePickerSurvey from '../components/ImagePickerSurvey';
import { ASSETS_URL } from '../../constants/Env';

export class DetailScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            data: null,
            no_urut: '',
            id_pel: '',
            date: null,
            mode: 'date',
            latitude: null,
            longitude: null,
            imageUpdate: null,
            imageCreate: null,
            desa: null,
            picker: [],
            surveyor: null,
            selectedSurveyor: '',
            ...this.props.navigation.state.params.data

        }
        console.log(this.state);
    }

    async componentDidMount() {
        this.setState({ loading: true });
        let surveyor = await Survey.getAllSurveyor();
        let picker = surveyor.Data.map((item, index) => {
            const a = { key: item.IdSurveyor, label: item.NamaSurveyor, value: item.IdSurveyor };
            return a;
        })
        this.setState({ picker: picker, loading: false })
    }

    hasLocationPermission = async () => {
        if (Platform.OS === 'ios' ||
            (Platform.OS === 'android' && Platform.Version < 23)) {
            return true;
        }

        const hasPermission = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        );

        if (hasPermission) return true;

        const status = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        );

        if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

        if (status === PermissionsAndroid.RESULTS.DENIED) {
            ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG);
        } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
            ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG);
        }

        return false;
    }

    _getLocation = async () => {
        const hasLocationPermission = await this.hasLocationPermission();

        if (!hasLocationPermission) return;

        this.setState({ loading: true }, () => {
            Geolocation.getCurrentPosition(
                (position) => {
                    this.setState({ latitude: position.coords.latitude.toString(), longitude: position.coords.longitude.toString(), loading: false });
                    console.log(position);
                },
                (error) => {
                    this.setState({ location: error, loading: false });
                    console.log(error);
                },
                { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50, forceRequestLocation: true }
            );
        });
    }

    _getLocationUpdate = async () => {
        const hasLocationPermission = await this.hasLocationPermission();

        if (!hasLocationPermission) return;

        this.setState({ loading: true }, () => {
            Geolocation.getCurrentPosition(
                (position) => {
                    this.setState({ Latitude: position.coords.latitude.toString(), Longitude: position.coords.longitude.toString(), loading: false });
                    console.log(position);
                },
                (error) => {
                    this.setState({ location: error, loading: false });
                    console.log(error);
                },
                { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50, forceRequestLocation: true }
            );
        });
    }

    async _changeState(value, parentState) {
        await this.setState({ [parentState]: value });
    }

    async submitForm() {
        const formData = new FormData();
        formData.append('Wilayah', this.state.wilayah)
        // formData.append('No', parseInt(this.state.no_urut))
        formData.append('Id_Pel', parseInt(this.state.id_pel))
        formData.append('Nama', this.state.nama)
        formData.append('IdSurveyor', this.state.surveyor)
        formData.append('Alamat', this.state.alamat)
        formData.append('Kwh', this.state.kwh)
        formData.append('Mcb', this.state.mcb)
        formData.append('Ground', this.state.ground)
        formData.append('Cospi', parseFloat(this.state.cospi))
        formData.append('Stand', parseFloat(this.state.stand))
        formData.append('Batas', this.state.batas)
        formData.append('Switch', this.state.switch)
        formData.append('Tanggal', this.state.date)
        formData.append('Lat', this.state.latitude)
        formData.append('Long', this.state.longitude)
        formData.append('Amp', parseFloat(this.state.amp))
        formData.append('Kd_Kabupaten', this.state.kabupaten)
        formData.append('Kd_Provinsi', this.state.provinsi)
        formData.append('Ket', this.state.keterangan)
        formData.append('Kd_Kecamatan', this.state.kecamatan)
        formData.append('Desa', this.state.desa)
        formData.append('Jumlah', parseInt(this.state.jumlah))
        formData.append('JumlahMCB', parseInt(this.state.jumlahmcb))
        formData.append('Kontaktor', this.state.kontraktor)
        formData.append('VoltAmpere', parseFloat(this.state.volt))
        formData.append('Daya', parseFloat(this.state.daya))
        formData.append('Watt', parseFloat(this.state.watt))
        formData.append('KabelInner', this.state.kabelInner)
        formData.append('KabelOuter', this.state.kabelOuter)
        formData.append('KondisiBox', {
            uri: this.state.imageCreate.uri,
            type: 'image/jpg',
            name: `${this.state.id_pel}.jpg`,
        })

        console.log(formData);

        const resp = await Survey.saveSurvey(formData).then(data => data).catch(err => err);
        // this.saveImage(this.state.id_pel, this.state.imageBase64Create)
        console.log(resp);
        if (resp == undefined) {
            Alert.alert('Error Add Data', 'Silahkan Cek Form Anda');
        } else {
            if (resp.Meta.Code == 200) {
                await this.props.navigation.pop(1);
                Alert.alert('Sukses Add Data', 'Success');
            } else {
                Alert.alert('Error Add Data', 'Silahkan Cek Form Anda');
            }
        }
    }

    async submitFormUpdate() {
        await this.setState({ loading: true });
        const formData = new FormData();
        formData.append('Wilayah', this.state.Wilayah)
        formData.append('No', parseInt(this.state.NoUrut))
        formData.append('Id_Pel', parseInt(this.state.IDPel))
        formData.append('Nama', this.state.Nama)
        formData.append('IdSurveyor', this.state.IdSurveyor)
        formData.append('Alamat', this.state.Alamat)
        formData.append('Kwh', this.state.KWhMeter)
        formData.append('Mcb', this.state.MCB)
        formData.append('Ground', this.state.Grounding)
        formData.append('Cospi', parseFloat(this.state.CosPhi))
        formData.append('Stand', parseFloat(this.state.StandKWH1))
        formData.append('Batas', this.state.PembatasDaya)
        formData.append('Switch', this.state.Switch)
        formData.append('Tanggal', this.state.Tanggal1)
        formData.append('Lat', this.state.Latitude)
        formData.append('Long', this.state.Longitude)
        formData.append('Amp', this.state.Ampere)
        formData.append('Kd_Kabupaten', this.state.KabupatenKota)
        formData.append('Kd_Provinsi', this.state.Provinsi)
        formData.append('Ket', this.state.Keterangan)
        formData.append('Kd_Kecamatan', this.state.Kecamatan)
        formData.append('Desa', this.state.Desa)
        formData.append('Jumlah', parseInt(this.state.JumlahLampu))
        formData.append('JumlahMCB', this.state.JumlahMCB)
        formData.append('Kontaktor', this.state.Kontaktor)
        formData.append('VoltAmpere', parseFloat(this.state.VoltAmpere))
        formData.append('Daya', this.state.Daya)
        formData.append('Watt', parseFloat(this.state.Watt))
        formData.append('KabelInner', this.state.KabelInner)
        formData.append('KabelOuter', this.state.KabelOuter)
        formData.append('KondisiBox', {
            uri: this.state.imageUpdate.uri,
            type: 'image/jpg',
            name: `${this.state.IDPel}.jpg`,
        })
        console.log(formData);
        const resp = await Survey.updateSurvey(formData).then(data => data).catch(err => {
            console.log('error message', err.message);
            return undefined;
        });
        // this.saveImage(this.state.IDPel, this.state.imageBase64Update)
        console.log(resp, 'resp');
        if (resp == undefined) {
            Alert.alert('Error Update', 'Silahkan Cek Form Anda');
        } else {
            if (resp.Meta.Code == 200) {
                await this.props.navigation.pop(1);
                Alert.alert('Sukses Update', 'Success');
            } else if (resp.Meta.Code == 500) {
                Alert.alert('Error Update', 'Silahkan Cek Form Anda');
            } else {
                Alert.alert('Error Update', 'Silahkan Cek Form Anda');
            }
        }
    }

    // async saveImage(namaFileBaru, value) {
    //     await writeFile(namaFileBaru, namaFileBaru, value);
    // }

    renderDataAvailable() {
        console.log('kondisi box', this.state.KondisiBox);
        const imageSource = {
            uri: ASSETS_URL + '/' + this.state.KondisiBox + '?time=' + new Date().getTime()
        }
        console.log(imageSource);

        return (
            <ScrollView style={{ margin: 10, flex: 1 }}>
                {
                    this.state.loading ? <ActivityIndicator /> :
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10, backgroundColor: '#BCE0FD' }}>
                            <Text style={{ flex: 1, fontWeight: 'bold' }}>Nama Surveyor</Text>
                            <RNPickerSelect
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                items={this.state.picker}
                                value={this.state.IdSurveyor}
                                onValueChange={(value) => this.setState({ IdSurveyor: value })}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                }
                <InputComponent
                    label='Nama Wilayah'
                    placeholder="Nama Wilayah"
                    placeholderTextColor="#2699FB"
                    value={this.state.Wilayah}
                    onChangeText={(text) => this.setState({ wilayah: text })}
                    disabled
                />
                <View style={{ flexDirection: 'row' }}>
                    <InputComponent
                        label='No Urut'
                        containerStyle={{ flex: 1, marginRight: 5 }}
                        placeholder="No Urut"
                        placeholderTextColor="#2699FB"
                        onChangeText={(text) => this.setState({ no_urut: text })}
                        value={this.state.NoUrut}
                        disabled
                    />
                    <InputComponent
                        label='IDPEL'
                        containerStyle={{ flex: 1, marginLeft: 5 }}
                        placeholder="IDPEL"
                        placeholderTextColor="#2699FB"
                        onChangeText={(text) => this.setState({ id_pel: text })}
                        value={this.state.IDPel}
                        disabled
                    />
                </View>
                <InputComponent
                    label='Nama Box'
                    placeholder="Nama Box"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ nama: text })}
                    value={this.state.Nama}
                    disabled
                />
                <InputComponent
                    label='Provinsi'
                    placeholder="Provinsi"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ provinsi: text })}
                    value={this.state.Provinsi}
                    disabled
                />
                <InputComponent
                    label='Kab/Kota'
                    placeholder="Kab/Kota"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ kabupaten: text })}
                    value={this.state.KabupatenKota}
                    disabled
                />
                <InputComponent
                    label='Kecamatan'
                    placeholder="Kecamatan"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ kecamatan: text })}
                    value={this.state.Kecamatan}
                    disabled
                />
                <InputComponent
                    label='Desa'
                    placeholder="Desa"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ desa: text })}
                    value={this.state.Desa}
                    disabled
                />
                <InputComponent
                    label='Alamat'
                    placeholder="Alamat"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ alamat: text })}
                    value={this.state.Alamat}
                    disabled
                />
                <View style={{ flexDirection: 'row' }}>
                    <InputComponent
                        label='Daya'
                        keyboardType='numeric'
                        containerStyle={{ flex: 1, marginRight: 5 }}
                        placeholder="Daya"
                        placeholderTextColor="#2699FB"
                        onChangeText={(text) => this.setState({ Daya: text })}
                        value={this.state.Daya}
                    />
                    <InputComponent
                        label='Jumlah Lampu'
                        keyboardType="numeric"
                        value={this.state.JumlahLampu}
                        containerStyle={{ flex: 1, marginLeft: 5 }}
                        placeholder="Jumlah lampu"
                        placeholderTextColor="#2699FB"
                        onChangeText={(text) => this.setState({ JumlahLampu: text })}
                    />
                </View>

                <DatePicker
                    getDate={this.state.Tanggal1}
                    setDate={(date) => this.setState({ Tanggal1: date })}
                />

                <View style={{ flex: 1, marginVertical: 10 }}>
                    <Text style={{ fontSize: 32, fontWeight: 'bold', marginVertical: 10 }}>Sisi PLN</Text>
                    <View style={{ backgroundColor: '#BCE0FD' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>KWH</Text>
                            <RNPickerSelect
                                value={this.state.KWhMeter}
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ KWhMeter: value })}
                                items={ketPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>Pembatas Daya</Text>
                            <RNPickerSelect
                                value={this.state.PembatasDaya}
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ PembatasDaya: value })}
                                items={ketPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>Kabel Inner</Text>
                            <RNPickerSelect
                                value={this.state.KabelInner}
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ KabelInner: value })}
                                items={kabelPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                    </View>
                </View>

                <View style={{ flex: 1, marginVertical: 10 }}>
                    <Text style={{ fontSize: 32, fontWeight: 'bold', marginVertical: 10 }}>Sisi PJU</Text>
                    <View style={{ backgroundColor: '#BCE0FD' }}>
                        <View style={{ paddingHorizontal: 10 }}>
                            <InputComponent
                                keyboardType="numeric"
                                label='Jumlah MCB'
                                placeholder="Jumlah MCB"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ JumlahMCB: text })}
                                value={this.state.JumlahMCB}
                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>MCB</Text>
                            <RNPickerSelect
                                value={this.state.MCB}
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ MCB: value })}
                                items={ketPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>Kontaktor</Text>
                            <RNPickerSelect
                                value={this.state.Kontaktor}
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ Kontaktor: value })}
                                items={ketPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>Switch</Text>
                            <RNPickerSelect
                                value={this.state.Switch}
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ Switch: value })}
                                items={switchPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>Grounding</Text>
                            <RNPickerSelect
                                value={this.state.Grounding}
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ Grounding: value })}
                                items={groundingPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>Kabel Outer</Text>
                            <RNPickerSelect
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ KabelOuter: value })}
                                value={this.state.KabelOuter}
                                items={kabelPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                    </View>
                </View>

                <View style={{ flex: 1, marginVertical: 10 }}>
                    <Text style={{ fontSize: 32, fontWeight: 'bold', marginVertical: 10 }}>Hasil Ukur</Text>
                    <View style={{ backgroundColor: '#BCE0FD' }}>
                        <Button
                            onPress={() => this._getLocationUpdate()}
                            containerStyle={{ flex: 1, margin: 10 }}
                            buttonStyle={{ padding: 10, marginRight: 5 }}
                            type='solid'
                            title='GET CURRENT LOCATION'
                            titleStyle={{ fontSize: 13 }}
                        />
                        {this.state.loading ? <ActivityIndicator size="large" color="#0000ff" /> :
                            <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                                <InputComponent
                                    label="latitude"
                                    containerStyle={{ flex: 1, }}
                                    value={this.state.Latitude}
                                    onChangeText={(text) => this.setState({ Latitude: text })}
                                    placeholder="Lat"
                                    placeholderTextColor="#2699FB"
                                    disabled
                                />
                                <InputComponent
                                    label="longitude"
                                    containerStyle={{ flex: 1, marginLeft: 5 }}
                                    value={this.state.Longitude}
                                    onChangeText={(text) => this.setState({ Longitude: text })}
                                    placeholder="Long"
                                    placeholderTextColor="#2699FB"
                                    disabled
                                />
                            </View>
                        }
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <InputComponent
                                label='Cosphi'
                                keyboardType="numeric"
                                containerStyle={{ flex: 1, marginLeft: 5 }}
                                placeholder="Cos phi"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ CosPhi: text })}
                                value={this.state.CosPhi}
                            />
                            <InputComponent
                                label='Ampere'
                                keyboardType="numeric"
                                containerStyle={{ flex: 1, marginLeft: 5 }}
                                placeholder="Ampere"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ Ampere: text })}
                                value={this.state.Ampere}

                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <InputComponent
                                label='Watt'
                                keyboardType="numeric"
                                containerStyle={{ flex: 1, marginLeft: 5 }}
                                placeholder="Watt"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ Watt: text })}
                                value={this.state.Watt}
                            />
                            <InputComponent
                                label='Volt Ampere'
                                keyboardType="numeric"
                                containerStyle={{ flex: 1, marginLeft: 5 }}
                                placeholder="Volt Ampere"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ VoltAmpere: text })}
                                value={this.state.VoltAmpere}
                            />
                        </View>

                        <View style={{ paddingHorizontal: 10 }}>
                            <InputComponent
                                keyboardType="numeric"
                                label='StandKwh'
                                placeholder="Stand KWH"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ StandKWH1: text })}
                                value={this.state.StandKWH1}
                            />
                            <InputComponent
                                label='Keterangan'
                                placeholder="Keterangan"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ Keterangan: text })}
                                value={this.state.Keterangan}
                            />
                        </View>
                    </View>
                </View>

                <ImagePickerSurvey
                    title='Upload Foto'
                    onSelectFiles={(value) => {
                        this.setState({ imageUpdate: value })
                    }}
                />

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image key={imageSource + new Date().now} source={this.state.imageUpdate != null ? this.state.imageUpdate : imageSource} style={{ width: 200, height: 200 }} />
                </View>

                <Button
                    onPress={() => this.submitFormUpdate()}
                    containerStyle={{ flex: 1 }}
                    buttonStyle={{ padding: 10, marginRight: 5 }}
                    type='solid'
                    title='SIMPAN'
                    titleStyle={{ fontSize: 13 }}
                />

            </ScrollView>
        )
    }

    renderDataNotAvailable() {
        const imageSource = {
            uri: ASSETS_URL + '/' + this.state.KondisiBox + '?time=' + new Date().getTime()
        }
        return (
            <ScrollView style={{ margin: 10, flex: 1 }}>
                {
                    this.state.loading ? <ActivityIndicator /> :
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10, backgroundColor: '#BCE0FD' }}>
                            <Text style={{ flex: 1, fontWeight: 'bold' }}>Nama Surveyor</Text>
                            <RNPickerSelect
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                items={this.state.picker}
                                value={this.state.surveyor}
                                onValueChange={(value) => this.setState({ surveyor: value })}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                }
                <InputComponent
                    label="Nama Wilayah"
                    placeholder="Nama Wilayah"
                    placeholderTextColor="#2699FB"
                    value={this.state.Wilayah}
                    onChangeText={(text) => this.setState({ wilayah: text })}
                />
                <InputComponent
                    keyboardType='numeric'
                    label="IDPEL"
                    placeholder="IDPEL"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ id_pel: text })}
                    value={this.state.IDPel}
                />
                <InputComponent
                    label="Nama Box"
                    placeholder="Nama Box"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ nama: text })}
                    value={this.state.Nama}
                />
                <InputComponent
                    label="Provinsi"
                    placeholder="Provinsi"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ provinsi: text })}
                    value={this.state.Provinsi}
                />
                <InputComponent
                    label="Kab/Kota"
                    placeholder="Kab/Kota"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ kabupaten: text })}
                    value={this.state.KabupatenKota}
                />
                <InputComponent
                    label="Kecamatan"
                    placeholder="Kecamatan"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ kecamatan: text })}
                    value={this.state.Kecamatan}
                />
                <InputComponent
                    label="Desa"
                    placeholder="Desa"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ desa: text })}
                    value={this.state.Desa}
                />
                <InputComponent
                    label="Alamat"
                    placeholder="Alamat"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ alamat: text })}
                    value={this.state.Alamat}
                />
                <View style={{ flexDirection: 'row' }}>
                    <InputComponent
                        label="Daya"
                        keyboardType="numeric"
                        containerStyle={{ flex: 1, marginRight: 5 }}
                        placeholder="Daya"
                        placeholderTextColor="#2699FB"
                        onChangeText={(text) => this.setState({ daya: text })}
                        value={this.state.Daya}
                    />
                    <InputComponent
                        label="Jumlah Lampu"
                        keyboardType="numeric"
                        value={this.state.JumlahLampu}
                        containerStyle={{ flex: 1, marginLeft: 5 }}
                        placeholder="Jumlah lampu"
                        placeholderTextColor="#2699FB"
                        onChangeText={(text) => this.setState({ jumlah: text })}
                    />
                </View>

                <DatePicker
                    getDate={this.state.date}
                    setDate={(date) => this.setState({ date: date })}
                />

                <View style={{ flex: 1, marginVertical: 10 }}>
                    <Text style={{ fontSize: 32, fontWeight: 'bold', marginVertical: 10 }}>Sisi PLN</Text>
                    <View style={{ backgroundColor: '#BCE0FD' }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>KWH</Text>
                            <RNPickerSelect
                                value={this.state.KWhMeter}
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ kwh: value })}
                                items={ketPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>

                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>Pembatas Daya</Text>
                            <RNPickerSelect
                                value={this.state.PembatasDaya}
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ batas: value })}
                                items={ketPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>Kabel Inner</Text>
                            <RNPickerSelect
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ kabelInner: value })}
                                items={kabelPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                    </View>
                </View>

                <View style={{ flex: 1, marginVertical: 10 }}>
                    <Text style={{ fontSize: 32, fontWeight: 'bold', marginVertical: 10 }}>Sisi PJU</Text>
                    <View style={{ backgroundColor: '#BCE0FD' }}>
                        <View style={{ paddingHorizontal: 10 }}>
                            <InputComponent
                                label="Jumlah MCB"
                                keyboardType="numeric"
                                placeholder="Jumlah MCB"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ jumlahmcb: text })}
                                value={this.state.JumlahMCB}
                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>MCB</Text>
                            <RNPickerSelect
                                value={this.state.MCB}
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ mcb: value })}
                                items={ketPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>Kontaktor</Text>
                            <RNPickerSelect
                                value={this.state.Kontaktor}
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ kontraktor: value })}
                                items={ketPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>Switch</Text>
                            <RNPickerSelect
                                value={this.state.Switch}
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ switch: value })}
                                items={switchPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>Grounding</Text>
                            <RNPickerSelect
                                value={this.state.Grounding}
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ ground: value })}
                                items={groundingPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <Text style={{ flex: 1 }}>Kabel Outer</Text>
                            <RNPickerSelect
                                style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                                onValueChange={(value) => this.setState({ kabelOuter: value })}
                                items={kabelPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                    </View>
                </View>

                <View style={{ flex: 1, marginVertical: 10 }}>
                    <Text style={{ fontSize: 32, fontWeight: 'bold', marginVertical: 10 }}>Hasil Ukur</Text>
                    <View style={{ backgroundColor: '#BCE0FD' }}>
                        <Button
                            onPress={() => this._getLocation()}
                            containerStyle={{ flex: 1, margin: 10 }}
                            buttonStyle={{ padding: 10, marginRight: 5 }}
                            type='solid'
                            title='GET CURRENT LOCATION'
                            titleStyle={{ fontSize: 13 }}
                        />
                        {this.state.loading ? <ActivityIndicator size="large" color="#0000ff" /> :
                            <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                                <InputComponent
                                    label="Latitude"
                                    containerStyle={{ flex: 1, }}
                                    value={this.state.latitude}
                                    placeholder="Lat"
                                    placeholderTextColor="#2699FB"
                                />
                                <InputComponent
                                    label="Longitude"
                                    containerStyle={{ flex: 1, marginLeft: 5 }}
                                    value={this.state.longitude}
                                    placeholder="Long"
                                    placeholderTextColor="#2699FB"
                                />
                            </View>
                        }
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <InputComponent
                                label='CosPhi'
                                keyboardType='numeric'
                                containerStyle={{ flex: 1, marginLeft: 5 }}
                                placeholder="Cos phi"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ cospi: text })}
                                value={this.state.CosPhi}
                            />
                            <InputComponent
                                label='Ampere'
                                keyboardType='numeric'
                                containerStyle={{ flex: 1, marginLeft: 5 }}
                                placeholder="Ampere"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ amp: text })}
                                value={this.state.Ampere}

                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                            <InputComponent
                                label='Watt'
                                keyboardType='numeric'
                                containerStyle={{ flex: 1, marginLeft: 5 }}
                                placeholder="Watt"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ watt: text })}
                                value={this.state.Watt}
                            />
                            <InputComponent
                                label='Volt Ampere'
                                keyboardType='numeric'
                                containerStyle={{ flex: 1, marginLeft: 5 }}
                                placeholder="Volt Ampere"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ volt: text })}
                                value={this.state.VoltAmpere}
                            />
                        </View>

                        <View style={{ paddingHorizontal: 10 }}>
                            <InputComponent
                                label='Stand KWH'
                                keyboardType='numeric'
                                placeholder="Stand KWH"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ stand: text })}
                                value={this.state.StandKWH1}
                            />
                            <InputComponent
                                label='Keterangan'
                                placeholder="Keterangan"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ keterangan: text })}
                                value={this.state.Keterangan}
                            />
                        </View>
                    </View>
                </View>

                <ImagePickerSurvey
                    title='Upload Foto'
                    onSelectFiles={(value) => {
                        this.setState({ imageCreate: value })
                    }}
                />

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image key={imageSource + new Date().now} source={this.state.imageCreate != null ? this.state.imageCreate : imageSource} style={{ width: 200, height: 200 }} />
                </View>

                <Button
                    onPress={() => this.submitForm()}
                    containerStyle={{ flex: 1 }}
                    buttonStyle={{ padding: 10, marginRight: 5 }}
                    type='solid'
                    title='SIMPAN'
                    titleStyle={{ fontSize: 13 }}
                />
            </ScrollView>
        )
    }

    render() {
        if (this.props.navigation.state.params.data == null) {
            return this.renderDataNotAvailable();
        } else {
            return this.renderDataAvailable();
        }
    }
}

export default DetailScreen
