import React, { Component } from 'react'
import { Text, View, ScrollView, TouchableOpacity, Platform, Alert } from 'react-native'
import { Button, Icon } from "react-native-elements";
import InputComponent from "../components/InputComponent";
import Survey from "../../api/Survey";
import DatePicker from '../components/DatePicker';

export class DetailScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: null,
            no_urut: '',
            id_pel: '',
            show: false,
            date: null,
            ...this.props.navigation.state.params.data
        }
        console.log(this.props);

    }


    async _changeState(value, parentState) {
        await this.setState({ [parentState]: value });
    }

    async submitForm() {
        const formData = new FormData();
        formData.append('Id_Pel', this.state.IDPel)
        formData.append('Tanggal2', this.state.Tanggal2)
        formData.append('StandKWH2', this.state.StandKWH2)
        console.log(formData);

        const resp = await Survey.saveSurvey2(formData).then(data => data).catch(err => err);
        console.log('resp', resp);

        if (resp == undefined) {
            Alert.alert('Error Update', 'Silahkan Cek Form Anda');
        } else {
            if (resp.Meta.Code == 200) {
                await this.props.navigation.pop(1);
                Alert.alert('Sukses Update', 'Success');
            } else {
                Alert.alert('Error Update', 'Silahkan Cek Form Anda');
            }
        }
    }

    renderDataAvailable() {
        console.log(this.state.Tanggal2);

        return (
            <ScrollView style={{ margin: 10, flex: 1 }}>
                <InputComponent
                    placeholder="Nama Wilayah"
                    placeholderTextColor="#2699FB"
                    value={this.state.Wilayah}
                    disabled
                />
                <View style={{ flexDirection: 'row' }}>
                    <InputComponent
                        containerStyle={{ flex: 1, marginRight: 5 }}
                        placeholder="No Urut"
                        placeholderTextColor="#2699FB"
                        onChangeText={(text) => this.setState({ no_urut: text })}
                        value={this.state.NoUrut}
                        disabled
                    />
                    <InputComponent
                        containerStyle={{ flex: 1, marginLeft: 5 }}
                        placeholder="IDPEL"
                        placeholderTextColor="#2699FB"
                        onChangeText={(text) => this.setState({ id_pel: text })}
                        value={this.state.IDPel}
                        disabled
                    />
                </View>
                <InputComponent
                    placeholder="Nama Box"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ nama: text })}
                    value={this.state.Nama}
                    disabled
                />

                <View>
                    <View>
                        <Text style={{ marginBottom: 5, fontWeight: "bold", color: '#2699FB' }}>Tanggal 1</Text>
                        <Button disabled title={this.state.Tanggal1} />
                    </View>
                </View>

                <View>
                    <View>
                        <Text style={{ marginBottom: 5, fontWeight: "bold", color: '#2699FB' }}>Tanggal 2</Text>
                        <DatePicker
                            getDate={this.state.Tanggal2}
                            setDate={(date) => this.setState({ Tanggal2: date })}
                        />
                    </View>
                </View>

                <InputComponent
                    keyboardType='numeric'
                    label="Stand Kwh 1"
                    placeholder="Stand Kwh 1"
                    placeholderTextColor="#2699FB"
                    value={this.state.StandKWH1}
                    disabled
                />

                <InputComponent
                    keyboardType='numeric'
                    label="Stand Kwh 2"
                    placeholder="Stand Kwh 2"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ StandKWH2: text })}
                    value={this.state.StandKWH2}
                />

                <Button
                    onPress={() => this.submitForm()}
                    containerStyle={{ flex: 1 }}
                    buttonStyle={{ padding: 10, marginRight: 5 }}
                    type='solid'
                    title='SIMPAN'
                    titleStyle={{ fontSize: 13 }}
                />

            </ScrollView>
        )
    }


    render() {
        return this.renderDataAvailable();
    }
}

export default DetailScreen
