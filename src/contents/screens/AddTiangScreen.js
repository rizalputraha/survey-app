import React, { Component } from 'react'
import { Text, View, TouchableOpacity, FlatList, ActivityIndicator, Alert } from 'react-native'
import { Input, Icon, Button } from "react-native-elements";
import SelectWilayah from '../components/SelectWilayah';
import InputComponent from '../components/InputComponent';
import Survey from "../../api/Survey";
import styles from "../../styles/Survey1Styles";

export class AddTiangScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchKey: null,
            loading: false,
            data: null,
            dataWilayah: null,
        }
    }

    static navigationOptions = ({ navigation }) => ({
        headerTitle: navigation.state.params.screen
    })

    async componentDidMount() {
        await this.setState({ loading: true });

        let wilayah = await Survey.getWilayah();
        let dataSurvey = await Survey.getAllSurvey();


        await this.setState({
            dataWilayah: wilayah.Data,
            data: dataSurvey.Data,
            loading: false,
        })
        console.log('wilayah', this.state.dataWilayah);
        await this.setState({ loading: false });
    }

    async onPressSearch() {
        if (this.state.wilayah == '') {
            Alert.alert(
                'Error',
                'Silahkan isi data wilayah',
                [
                    { text: 'OK', onPress: () => console.log('OK Pressed') },
                ],
            );
        } else {
            await this.setState({ loading: true });
            const formData = new FormData();
            if (this.state.searchKey == '') {
                formData.append('Wilayah', this.state.wilayah);
                let dataSurvey = await Survey.getSurvey(formData);
                await this.setState({
                    loading: false,
                    data: dataSurvey.Data
                })
            } else {
                formData.append('Wilayah', this.state.wilayah);
                formData.append('Id_Pel', parseInt(this.state.searchKey));
                let dataSurvey = await Survey.getSurvey(formData);
                console.log(dataSurvey);
                await this.setState({
                    loading: false,
                    data: dataSurvey.Data
                })
            }
        }
    }

    async onPressReset() {
        await this.setState({ loading: true });

        let wilayah = await Survey.getWilayah();
        let dataSurvey = await Survey.getAllSurvey();

        await this.setState({
            dataWilayah: wilayah.Data,
            data: dataSurvey.Data,
            searchKey: '',
            wilayah: '',
            loading: false,
        })
        await this.setState({ loading: false });
    }

    async _changeState(value, parentState) {
        await this.setState({ [parentState]: value });
    }

    renderItem(item) {
        return (
            <TouchableOpacity
                style={styles.containerItem}
                onPress={() => this.props.navigation.push('DetailTiang', { data: item })}
            >
                <Text>{item.IDPel}</Text>
                <Text>{item.Nama}</Text>
                <Text>{item.Wilayah}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            this.state.loading && <ActivityIndicator size="large" color="#0000ff" /> ||
            <View style={{ padding: 10, flex: 1 }}>
                <View style={styles.containerItem}
                    onPress={() => this.props.navigation.push('DetailScreen2', { data: item })}
                >
                    <Text>No Urut</Text>
                    <Text>IDPel</Text>
                    <Text>Nama</Text>
                    <Text>Wilayah</Text>
                </View>

                <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                    <Button
                        containerStyle={{ flex: 1 }}
                        buttonStyle={{ padding: 10, marginRight: 5 }}
                        type='outline'
                        title='Tambah Tiang'
                        onPress={() => this.onPressSearch()}
                        titleStyle={{ fontSize: 13 }}
                    />
                </View>
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    data={this.state.data}
                    renderItem={({ item }) => this.renderItem(item)}
                />
            </View>
        )
    }
}

export default AddTiangScreen
