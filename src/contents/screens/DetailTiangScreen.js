import React, { Component } from 'react'
import { Text, View, ScrollView, Alert, Platform, ActivityIndicator, PermissionsAndroid, Image } from 'react-native'
import { Button, Icon } from "react-native-elements";
import InputComponent from "../components/InputComponent";
import Survey from "../../api/Survey";
import RNPickerSelect from 'react-native-picker-select';
import Geolocation from "react-native-geolocation-service";
import { jenisTiangPicker, jenisKabelPicker, jenisLampuPicker } from '../../constants/picker'
import ImagePickerSurvey from '../components/ImagePickerSurvey';
import { loadFile, writeFile } from '../../helper/helper'
import { ASSETS_URL } from '../../constants/Env';

export class DetailTiangScreen extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: null,
            namaBox: '',
            noTiang: '',
            idPel: '',
            daya: '',
            amp: '',
            ket: '',

            latitude: null,
            longitude: null,

            imageCreate: null,
            imageUpdate: null,

            ...this.props.navigation.state.params.data
        }
        console.log(this.state);

    }

    async componentDidMount() {
        // const result = await loadFile(this.state.IDPel);
        // this.setState({
        //     imageBase64Update: result
        // })
    }

    hasLocationPermission = async () => {
        if (Platform.OS === 'ios' ||
            (Platform.OS === 'android' && Platform.Version < 23)) {
            return true;
        }

        const hasPermission = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        );

        if (hasPermission) return true;

        const status = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        );

        if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

        if (status === PermissionsAndroid.RESULTS.DENIED) {
            ToastAndroid.show('Location permission denied by user.', ToastAndroid.LONG);
        } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
            ToastAndroid.show('Location permission revoked by user.', ToastAndroid.LONG);
        }

        return false;
    }

    _getLocation = async () => {
        const hasLocationPermission = await this.hasLocationPermission();

        if (!hasLocationPermission) return;

        this.setState({ loading: true }, () => {
            Geolocation.getCurrentPosition(
                (position) => {
                    this.setState({ Lat: position.coords.latitude.toString(), Lng: position.coords.longitude.toString(), loading: false });
                    console.log(position);
                },
                (error) => {
                    this.setState({ location: error, loading: false });
                    console.log(error);
                },
                { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50, forceRequestLocation: true }
            );
        });
    }

    _getLocation2 = async () => {
        const hasLocationPermission = await this.hasLocationPermission();

        if (!hasLocationPermission) return;

        this.setState({ loading: true }, () => {
            Geolocation.getCurrentPosition(
                (position) => {
                    this.setState({ latitude: position.coords.latitude.toString(), longitude: position.coords.longitude.toString(), loading: false });
                    console.log(position);
                },
                (error) => {
                    this.setState({ location: error, loading: false });
                    console.log(error);
                },
                { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000, distanceFilter: 50, forceRequestLocation: true }
            );
        });
    }

    async _changeState(value, parentState) {
        await this.setState({ [parentState]: value });
    }

    async submitForm() {
        const formData = new FormData();
        formData.append('NoTiang', parseInt(this.state.noTiang))
        formData.append('IDPel', this.state.IDPel)
        formData.append('Nama', this.state.Nama)
        formData.append('JenisTiang', this.state.jenisTiang)
        formData.append('JenisKabel', this.state.jenisKabel)
        formData.append('Lat', this.state.latitude)
        formData.append('Lng', this.state.longitude)
        formData.append('JumlahLampu', this.state.jumlahLampu)
        formData.append('JenisLampu', this.state.jenisLampu)
        formData.append('DayaLampu', this.state.daya)
        formData.append('VALampu', this.state.amp)
        formData.append('KondisiTiang', {
            uri: this.state.imageCreate.uri,
            type: 'image/jpg',
            name: `${this.state.IDPel}.jpg`,
        })
        formData.append('KeteranganTiang', this.state.ket)
        console.log(formData);
        const resp = await Survey.addTiang(formData).catch(err => console.log(err));
        // save storage
        // this.saveImage(this.state.IDPel + '-' + this.state.NoTiang, this.state.imageBase64Create);
        if (resp == undefined) {
            Alert.alert('Error Add Data', 'Silahkan Cek Form Anda');
        } else {
            if (resp.Meta.Code == 200) {
                await this.props.navigation.pop(1);
                Alert.alert('Sukses Add Data', 'Success');
            } else {
                Alert.alert('Error Add Data', 'Silahkan Cek Form Anda');
            }
        }
    }

    async submitFormUpdate() {
        const formData = new FormData();
        formData.append('IDTiang', parseInt(this.state.IDTiang))
        formData.append('NoTiang', parseInt(this.state.NoTiang))
        formData.append('IDPel', parseInt(this.state.IDPel))
        formData.append('Nama', this.state.Nama)
        formData.append('JenisTiang', this.state.JenisTiang)
        formData.append('JenisKabel', this.state.JenisKabel)
        formData.append('Lat', this.state.Lat)
        formData.append('Lng', this.state.Lng)
        formData.append('JumlahLampu', this.state.JumlahLampu)
        formData.append('JenisLampu', this.state.JenisLampu)
        formData.append('DayaLampu', this.state.DayaLampu)
        formData.append('VALampu', this.state.VALampu)
        formData.append('KondisiTiang', {
            uri: this.state.imageUpdate.uri,
            type: 'image/jpg',
            name: `${this.state.IDPel}-${this.state.NoTiang}.jpg`,
        })
        formData.append('KeteranganTiang', this.state.KeteranganTiang)
        console.log(formData);
        const resp = await Survey.updateTiang(formData);
        // save storage
        // this.saveImage(this.state.IDPel + this.state.NoTiang, this.state.imageBase64Update);
        if (resp == undefined) {
            Alert.alert('Error Update Data', 'Silahkan Cek Form Anda');
        } else {
            if (resp.Meta.Code == 200) {
                await this.props.navigation.pop(1);
                Alert.alert('Sukses Update Data', 'Success');
            } else {
                Alert.alert('Error Update Data', 'Silahkan Cek Form Anda');
            }
        }
    }

    // async saveImage(namaFileBaru, value) {
    //     await writeFile(namaFileBaru, value) 
    // }

    renderDataAvailable() {
        const imageSource = {
            uri: ASSETS_URL + '/tiang/' + this.state.KondisiTiang + '?time=' + new Date().getTime()
        }
        console.log('image source', imageSource);

        return (
            <ScrollView style={{ margin: 10, flex: 1 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <InputComponent
                        label='Nama Box'
                        containerStyle={{ flex: 1, }}
                        placeholder="Nama Box"
                        placeholderTextColor="#2699FB"
                        onChangeText={(text) => this.setState({ Nama: text })}
                        value={this.state.Nama}
                    />
                    <InputComponent
                        label='No Tiang'
                        containerStyle={{ flex: 1, }}
                        placeholder="No Tiang"
                        placeholderTextColor="#2699FB"
                        onChangeText={(text) => this.setState({ NoTiang: text })}
                        value={this.state.NoTiang}
                    />
                </View>
                <InputComponent
                    label='ID Pel'
                    containerStyle={{ padding: 10 }}
                    placeholder="IDPel"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ IDPel: text })}
                    value={this.state.IDPel}
                />

                <View style={{ flex: 1, marginVertical: 5 }}>
                    <Text style={{ fontSize: 32, fontWeight: 'bold', marginVertical: 10 }}>Hasil Ukur Tiang</Text>
                    <View style={{ backgroundColor: '#BCE0FD' }}>
                        <Button
                            onPress={() => this._getLocation()}
                            containerStyle={{ flex: 1, margin: 10 }}
                            buttonStyle={{ padding: 10, marginRight: 5 }}
                            type='solid'
                            title='GET CURRENT LOCATION'
                            titleStyle={{ fontSize: 13 }}
                        />
                        {this.state.loading ? <ActivityIndicator size="large" color="#0000ff" /> :
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <InputComponent
                                    containerStyle={{ flex: 1, }}
                                    value={this.state.Lat}
                                    placeholder="Lat"
                                    placeholderTextColor="#2699FB"
                                />
                                <InputComponent
                                    containerStyle={{ flex: 1, marginLeft: 5 }}
                                    value={this.state.Lng}
                                    placeholder="Long"
                                    placeholderTextColor="#2699FB"
                                />
                            </View>
                        }
                        <View >
                            <Text style={{ marginLeft: 10, marginTop: 10 }}>Jenis Tiang</Text>
                            <RNPickerSelect
                                value={this.state.JenisTiang}
                                style={{ iconContainer: { top: 20, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', margin: 10 } }}
                                onValueChange={(value) => this.setState({ JenisTiang: value })}
                                items={jenisTiangPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>

                        <View >
                            <Text style={{ marginLeft: 10, marginTop: 10 }}>Jenis Kabel</Text>
                            <RNPickerSelect
                                value={this.state.JenisKabel}
                                style={{ iconContainer: { top: 20, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', margin: 10 } }}
                                onValueChange={(value) => this.setState({ JenisKabel: value })}
                                items={jenisKabelPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                    </View>
                </View>

                <View style={{ flex: 1, marginVertical: 10 }}>
                    <Text style={{ fontSize: 32, fontWeight: 'bold', marginVertical: 10 }}>Lampu Terpasang</Text>
                    <View style={{ backgroundColor: '#BCE0FD' }}>
                        <View style={{ paddingHorizontal: 10 }}>
                            <InputComponent
                                keyboardtype='numeric'
                                label='Jumlah Lampu'
                                placeholder="Jumlah Lampu"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ JumlahLampu: text })}
                                value={this.state.JumlahLampu}
                            />
                            <View >
                                <Text style={{ marginLeft: 10, marginTop: 10 }}>Jenis Lampu</Text>
                                <RNPickerSelect
                                    value={this.state.JenisLampu}
                                    style={{ iconContainer: { top: 20, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', margin: 10 } }}
                                    onValueChange={(value) => this.setState({ JenisLampu: value })}
                                    items={jenisLampuPicker}
                                    Icon={
                                        () => <Icon name="arrow-drop-down" />
                                    }
                                />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 10 }}>
                            <InputComponent
                                keyboardType='numeric'
                                label="Daya"
                                containerStyle={{ flex: 1, }}
                                placeholder="Daya"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ DayaLampu: text })}
                                value={this.state.DayaLampu}
                            />
                            <InputComponent
                                keyboardType='numeric'
                                label="Volt Ampere"
                                containerStyle={{ flex: 1, marginLeft: 5 }}
                                placeholder="Volt Ampere"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ VALampu: text })}
                                value={this.state.VALampu}

                            />
                        </View>
                        <InputComponent
                            label='Keterangan'
                            containerStyle={{ margin: 5 }}
                            placeholder="Keterangan"
                            placeholderTextColor="#2699FB"
                            onChangeText={(text) => this.setState({ KeteranganTiang: text })}
                            value={this.state.KeteranganTiang}
                        />
                    </View>
                </View>

                <ImagePickerSurvey
                    title='Upload Photo'
                    onSelectFiles={(value) => this.setState({ imageUpdate: value })}
                />

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={this.state.imageUpdate != null ? this.state.imageUpdate : imageSource} style={{ width: 200, height: 200 }} />
                </View>

                <Button
                    onPress={() => this.submitFormUpdate()}
                    containerStyle={{ flex: 1 }}
                    buttonStyle={{ padding: 10, marginRight: 5 }}
                    type='solid'
                    title='SIMPAN'
                    titleStyle={{ fontSize: 13 }}
                />
            </ScrollView>
        )
    }

    renderDataNotAvailable() {
        return (
            <ScrollView style={{ margin: 10, flex: 1 }}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <InputComponent
                        value={this.state.Nama}
                        label="Nama Box"
                        containerStyle={{ flex: 1, }}
                        placeholder="Nama Box"
                        placeholderTextColor="#2699FB"
                        onChangeText={(text) => this.setState({ namaBox: text })}
                    />
                    <InputComponent
                        label="No Tiang"
                        containerStyle={{ flex: 1, }}
                        placeholder="No Tiang"
                        placeholderTextColor="#2699FB"
                        onChangeText={(text) => this.setState({ noTiang: text })}
                    />
                </View>
                <InputComponent
                    value={this.state.IDPel}
                    containerStyle={{ padding: 10 }}
                    placeholder="IDPel"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ idPel: text })}
                />

                <View style={{ flex: 1, marginVertical: 5 }}>
                    <Text style={{ fontSize: 32, fontWeight: 'bold', marginVertical: 10 }}>Hasil Ukur Tiang</Text>
                    <View style={{ backgroundColor: '#BCE0FD' }}>
                        <Button
                            onPress={() => this._getLocation2()}
                            containerStyle={{ flex: 1, margin: 10 }}
                            buttonStyle={{ padding: 10, marginRight: 5 }}
                            type='solid'
                            title='GET CURRENT LOCATION'
                            titleStyle={{ fontSize: 13 }}
                        />
                        {this.state.loading ? <ActivityIndicator size="large" color="#0000ff" /> :
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <InputComponent
                                    containerStyle={{ flex: 1, }}
                                    value={this.state.latitude}
                                    placeholder="Lat"
                                    placeholderTextColor="#2699FB"
                                />
                                <InputComponent
                                    containerStyle={{ flex: 1, marginLeft: 5 }}
                                    value={this.state.longitude}
                                    placeholder="Long"
                                    placeholderTextColor="#2699FB"
                                />
                            </View>
                        }
                        <View >
                            <Text style={{ marginLeft: 10, marginTop: 10 }}>Jenis Tiang</Text>
                            <RNPickerSelect
                                style={{ iconContainer: { top: 20, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', margin: 10 } }}
                                onValueChange={(value) => this.setState({ jenisTiang: value })}
                                items={jenisTiangPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                        <View >
                            <Text style={{ marginLeft: 10, marginTop: 10 }}>Jenis Kabel</Text>
                            <RNPickerSelect
                                style={{ iconContainer: { top: 20, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', margin: 10 } }}
                                onValueChange={(value) => this.setState({ jenisKabel: value })}
                                items={jenisKabelPicker}
                                Icon={
                                    () => <Icon name="arrow-drop-down" />
                                }
                            />
                        </View>
                    </View>
                </View>

                <View style={{ flex: 1, marginVertical: 10 }}>
                    <Text style={{ fontSize: 32, fontWeight: 'bold', marginVertical: 10 }}>Lampu Terpasang</Text>
                    <View style={{ backgroundColor: '#BCE0FD' }}>
                        <View style={{ paddingHorizontal: 10 }}>
                            <InputComponent
                                keyboardtype='numeric'
                                label="Jumlah Lampu"
                                placeholder="Jumlah Lampu"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ jumlahLampu: text })}
                                value={this.state.JumlahMCB}
                            />
                            <View >
                                <Text style={{ marginLeft: 10, marginTop: 10 }}>Jenis Lampu</Text>
                                <RNPickerSelect
                                    style={{ iconContainer: { top: 20, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', margin: 10 } }}
                                    onValueChange={(value) => this.setState({ jenisLampu: value })}
                                    items={jenisLampuPicker}
                                    Icon={
                                        () => <Icon name="arrow-drop-down" />
                                    }
                                />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 10 }}>
                            <InputComponent
                                keyboardtype='numeric'
                                label="Daya"
                                containerStyle={{ flex: 1, }}
                                placeholder="Daya"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ daya: text })}
                                value={this.state.CosPhi}
                            />
                            <InputComponent
                                keyboardtype='numeric'
                                label="Volt Ampere"
                                containerStyle={{ flex: 1, marginLeft: 5 }}
                                placeholder="Volt Ampere"
                                placeholderTextColor="#2699FB"
                                onChangeText={(text) => this.setState({ amp: text })}
                                value={this.state.Amphere}

                            />
                        </View>
                        <InputComponent
                            label="Keterangan"
                            containerStyle={{ margin: 5 }}
                            placeholder="Keterangan"
                            placeholderTextColor="#2699FB"
                            onChangeText={(text) => this.setState({ ket: text })}
                            value={this.state.Amphere}
                        />
                    </View>
                </View>

                <ImagePickerSurvey
                    title='Upload Photo'
                    onSelectFiles={(value) => this.setState({ imageCreate: value })}
                />

                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={this.state.imageCreate} style={{ width: 200, height: 200 }} />
                </View>

                <Button
                    onPress={() => this.submitForm()}
                    containerStyle={{ flex: 1 }}
                    buttonStyle={{ padding: 10, marginRight: 5 }}
                    type='solid'
                    title='SIMPAN'
                    titleStyle={{ fontSize: 13 }}
                />
            </ScrollView>
        )
    }

    render() {
        if (this.props.navigation.state.params.data.NoTiang != null) {
            return this.renderDataAvailable();
        } else {
            return this.renderDataNotAvailable();
        }
    }
}

export default DetailTiangScreen
