import React, { Component } from 'react'
import { Text, FlatList, ActivityIndicator, TouchableOpacity, SafeAreaView } from 'react-native'
import ActionButton from 'react-native-action-button';
import { withNavigation } from 'react-navigation';

import Survey from '../../api/Survey';
import styles from "../../styles/Survey1Styles";

export class Surveyor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            surveyor: null,
            loading: false,
        }
    }

    fetchData() {
        const { navigation } = this.props;
        this.focusListener = navigation.addListener('didFocus', async () => {
            await this.setState({ loading: true });

            let dataSurveyor = await Survey.getAllSurveyor().then(data => data).catch(err => err);

            await this.setState({
                surveyor: dataSurveyor.Data,
                loading: false,
            })
            await this.setState({ loading: false });
        });
    }

    componentDidMount() {
        this.fetchData();
    }

    componentWillUnmount() {
        // Remove the event listener
        this.focusListener.remove();
    }

    renderItem(item) {
        return (
            <TouchableOpacity
                style={styles.containerItem}
                onPress={() => this.props.navigation.push('DetailSurveyor', { data: item })}
            >
                <Text>{item.NamaSurveyor}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                {
                    this.state.loading ? <ActivityIndicator /> :
                        <FlatList
                            keyExtractor={(item, index) => index.toString()}
                            data={this.state.surveyor}
                            renderItem={({ item }) => this.renderItem(item)}
                        />
                }
                <ActionButton onPress={() => this.props.navigation.push('DetailSurveyor', { data: null })} fixNativeFeedbackRadius={true} shadowStyle={{ elevation: 3 }} buttonColor="#009688" />
            </SafeAreaView>
        )
    }
}

export default withNavigation(Surveyor)
