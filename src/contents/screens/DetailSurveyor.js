import React, { Component } from 'react'
import { Text, View, Alert } from 'react-native'
import InputComponent from "../components/InputComponent";
import { Button } from 'react-native-elements'
import Survey from '../../api/Survey';

export class DetailSurveyor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            ...this.props.navigation.state.params.data
        }
    }

    async submitForm() {
        const formData = new FormData();
        formData.append('Nama', this.state.name);
        const resp = await Survey.addSurveyor(formData);
        console.log('respon api: ', resp);

        if (resp == undefined) {
            Alert.alert('Error Update', 'Silahkan Cek Form Anda');
        } else {
            if (resp.Meta.Code == 200) {
                await this.props.navigation.pop(1);
                Alert.alert('Sukses Update', 'Success');
            } else {
                Alert.alert('Error Update', 'Silahkan Cek Form Anda');
            }
        }
    }

    async submitFormUpdate() {
        const formData = new FormData();
        formData.append('Id', this.state.IdSurveyor);
        formData.append('Nama', this.state.NamaSurveyor);
        const resp = await Survey.updateSurveyor(formData);
        console.log('respon api: ', resp);

        if (resp == undefined) {
            Alert.alert('Error Update', 'Silahkan Cek Form Anda');
        } else {
            if (resp.Meta.Code == 200) {
                await this.props.navigation.pop(1);
                Alert.alert('Sukses Update', 'Success');
            } else {
                Alert.alert('Error Update', 'Silahkan Cek Form Anda');
            }
        }
    }

    renderDataAvailable() {
        return (
            <View style={{ margin: 10 }}>
                <InputComponent
                    label='Nama Surveyor'
                    placeholder="Nama Surveyor"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ NamaSurveyor: text })}
                    value={this.state.NamaSurveyor}
                />
                <Button
                    onPress={() => this.submitFormUpdate()}
                    buttonStyle={{ padding: 10, marginRight: 5 }}
                    type='solid'
                    title='SIMPAN'
                    titleStyle={{ fontSize: 13 }}
                />
            </View>
        )
    }

    renderDataNotAvailable() {
        return (
            <View style={{ margin: 10 }}>
                <InputComponent
                    label='Nama Surveyor'
                    placeholder="Nama Surveyor"
                    placeholderTextColor="#2699FB"
                    onChangeText={(text) => this.setState({ name: text })}
                    value={this.state.NamaSurveyor}
                />
                <Button
                    onPress={() => this.submitForm()}
                    buttonStyle={{ padding: 10, marginRight: 5 }}
                    type='solid'
                    title='SIMPAN'
                    titleStyle={{ fontSize: 13 }}
                />
            </View>
        )
    }

    render() {
        if (this.props.navigation.state.params.data == null) {
            return this.renderDataNotAvailable();
        } else {
            return this.renderDataAvailable();
        }
    }
}

export default DetailSurveyor
