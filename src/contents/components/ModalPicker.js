import React, { Component } from 'react'
import { Text, View, Modal, TouchableOpacity, Image, ScrollView, Dimensions, StyleSheet, PermissionsAndroid } from 'react-native'
import { Icon, Button } from "react-native-elements";
import ImagePicker from "react-native-image-picker";
import HeaderComponent from "./HeaderComponent";

const { height, width } = Dimensions.get('window');

export class ModalPicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false,
      showImage: null,
    }
  }

  async componentDidMount() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: 'Cool Photo App Camera Permission',
          message:
            'Cool Photo App needs access to your camera ' +
            'so you can take awesome pictures.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the camera');
      } else {
        console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  setImage() {
    if (this.state.showImage === null) Alert.alert('Please select file');
    this.props.onSelectFiles(this.state.showImage);
    console.log('image', this.state.showImage);

    this.setModalVisible(!this.state.isModalVisible);
  }

  openGallery() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      title: 'Select Avatar',
      customButtons: [{ name: 'fb', title: 'Choose Photo From Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      }
    }
    ImagePicker.launchImageLibrary(options, (response) => {
      console.log("Response : ", response);

      if (response.didCancel) {
        console.log("user cancelled image picker");
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = 'data:image/jpeg;base64,' + response.data;
        // const source = { uri: response.uri };

        this.setState({
          showImage: source,
        });

      }
    });
  }


  openCamera() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      title: 'Select Avatar',
      customButtons: [{ name: 'fb', title: 'Choose Photo From Facebook' }],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      }
    }
    ImagePicker.launchCamera(options, (response) => {
      console.log("Response : ", response);

      if (response.didCancel) {
        console.log("user cancelled image picker");
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        // const source = { uri: response.uri };

        // You can also display the image using data:
        const source = 'data:image/jpeg;base64,' + response.data;

        this.setState({
          showImage: source,
        });

      }
    });
  }

  setModalVisible(state) {
    this.setState({
      isModalVisible: state
    })
  }

  renderButton() {
    return (
      <TouchableOpacity onPress={() => this.setModalVisible(!this.state.isModalVisible)} style={styles.buttonModal} >
        <Icon containerStyle={{ marginRight: 10 }} color="white" name="backup"></Icon>
        <Text style={styles.textButtonModal}>{this.props.title}</Text>
      </TouchableOpacity>
    )
  }

  renderContentModal() {
    return (
      <View>
        <Text style={{ paddingHorizontal: 15, paddingTop: 10, fontSize: 14, }}>Upload Dari</Text>
        <Button
          onPress={() => this.openGallery()}
          title='Gallery'
        />
        <Text style={{ paddingHorizontal: 15, fontSize: 14, }}>Upload Dari</Text>
        <Button
          onPress={() => this.openCamera()}
          title='Camera'
        />
      </View>
    )
  }

  renderResultImage() {
    return (
      this.state.showImage != null ?
        <View style={styles.contentModal}>
          <View style={styles.container}>
            <Image
              resizeMode={'contain'}
              source={{ uri: this.state.showImage }}
              style={{ width: width, height: 0.4 * height }}
            />
          </View>
        </View>
        : <View></View>
    )
  }

  renderModal() {
    return (
      <Modal
        animationType='slide'
        transparent={false}
        visible={this.state.isModalVisible}
        onRequestClose={() => this.setModalVisible(!this.state.isModalVisible)}
      >
        <ScrollView>
          <HeaderComponent title={this.props.title} pushBackButtonHeader={() => this.setModalVisible(!this.state.isModalVisible)} />
          {this.renderContentModal()}
          {this.renderResultImage()}
          <Button onPress={() => this.setImage()} title='Save' />
        </ScrollView>
      </Modal>
    )
  }

  render() {
    return (
      <View>
        {this.renderButton()}
        {this.renderModal()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  buttonModal: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: '#2196f3',
    margin: 10,
    padding: 10,
    borderRadius: 8,
    elevation: 2
  },
  textButtonModal: {
    color: 'white',
    fontWeight: 'bold'
  },
  container: {
    flex: 1,
    backgroundColor: "#FFF"
  },
  contentModal: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: "#FFF",
    marginHorizontal: 10,
  },
})

export default ModalPicker
