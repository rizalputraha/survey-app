import React, { Component } from 'react'
import { Input, Icon } from "react-native-elements";

export class InputComponent extends Component {
    render() {
        return (
            <Input
                value={this.props.value}
                containerStyle={[this.props.containerStyle, { paddingHorizontal: 0, marginVertical: 10 }]}
                placeholderTextColor={this.props.placeholderTextColor ? this.props.placeholderTextColor : '#BCE0FD'}
                inputStyle={{ padding: 5, color: '#2699FB' }}
                inputContainerStyle={{ borderWidth: 1, borderColor: '#BCE0FD', backgroundColor: 'white', padding: 5 }}
                placeholder={this.props.placeholder}
                rightIconContainerStyle={{ paddingRight: 15 }}
                rightIcon={this.props.rightIcon}
                onChangeText={(text) => this.props.onChangeText(text)}
                {...this.props}
            />
        )
    }
}

export default InputComponent
