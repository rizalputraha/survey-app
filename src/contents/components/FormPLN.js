import React, { Component } from 'react'
import { Text, View } from 'react-native'
import InputComponent from "./InputComponent";
import RNPickerSelect from 'react-native-picker-select';
import { ketPicker } from '../../constants/picker'
import { Icon } from 'react-native-elements';

export class FormPLN extends Component {
    render() {
        console.log("ketpicker", ketPicker);

        return (
            <View style={{ flex: 1, marginVertical: 10 }}>
                <Text style={{ fontSize: 32, fontWeight: 'bold', marginVertical: 10 }}>Sisi PLN</Text>
                <View style={{ backgroundColor: '#BCE0FD' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                        <Text style={{ flex: 1 }}>KWH</Text>
                        <RNPickerSelect
                            style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                            onValueChange={(value) => this.props.valueChange(value)}
                            items={ketPicker}
                            Icon={
                                () => <Icon name="arrow-drop-down" />
                            }
                        />
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                        <Text style={{ flex: 1 }}>Pembatas Daya</Text>
                        <RNPickerSelect
                            style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                            onValueChange={(value) => this.props.valueChange(value)}
                            items={ketPicker}
                            Icon={
                                () => <Icon name="arrow-drop-down" />
                            }
                        />
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                        <Text style={{ flex: 1 }}>Kabel Inner</Text>
                        <RNPickerSelect
                            style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                            onValueChange={(value) => this.props.valueChange(value)}
                            items={ketPicker}
                            Icon={
                                () => <Icon name="arrow-drop-down" />
                            }
                        />
                    </View>
                </View>
            </View>
        )
    }
}

export default FormPLN
