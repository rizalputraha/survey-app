import React, { Component } from 'react'
import { Text, View, Platform } from 'react-native'
import { Button, Icon } from "react-native-elements";
import Geolocation from 'react-native-geolocation-service';
import InputComponent from "./InputComponent";
import RNPickerSelect from 'react-native-picker-select'
import { ketPicker } from '../../constants/picker'

export class FormHasilUkur extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      latitude: '',
      longitude: '',
    }
  }



  render() {
    return (
      <View style={{ flex: 1, marginVertical: 10 }}>
        <Text style={{ fontSize: 32, fontWeight: 'bold', marginVertical: 10 }}>Hasil Ukur</Text>
        <View style={{ backgroundColor: '#BCE0FD' }}>
          <Button
            onPress={() => this._getLocation()}
            containerStyle={{ flex: 1, margin: 10 }}
            buttonStyle={{ padding: 10, marginRight: 5 }}
            type='solid'
            title='GET CURRENT LOCATION'
            titleStyle={{ fontSize: 13 }}
          />
          <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
            <InputComponent
              containerStyle={{ flex: 1, }}
              value={this.state.latitude}
              placeholder="Lat"
              placeholderTextColor="#2699FB"
            />
            <InputComponent
              containerStyle={{ flex: 1, marginLeft: 5 }}
              value={this.state.longitude}
              placeholder="Long"
              placeholderTextColor="#2699FB"
            />
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
            <InputComponent
              containerStyle={{ flex: 1, marginLeft: 5 }}
              placeholder="Cos phi"
              placeholderTextColor="#2699FB"
            />
            <InputComponent
              containerStyle={{ flex: 1, marginLeft: 5 }}
              placeholder="Ampere"
              placeholderTextColor="#2699FB"
            />
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
            <InputComponent
              containerStyle={{ flex: 1, marginLeft: 5 }}
              placeholder="Watt"
              placeholderTextColor="#2699FB"
            />
            <InputComponent
              containerStyle={{ flex: 1, marginLeft: 5 }}
              placeholder="Volt Ampere"
              placeholderTextColor="#2699FB"
            />
          </View>

          <View style={{ paddingHorizontal: 10 }}>
            <InputComponent
              placeholder="Stand KWH"
              placeholderTextColor="#2699FB" />
            <RNPickerSelect
              placeholder={{ label: 'Pilih Keterangan' }}
              style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginBottom: 10 } }}
              onValueChange={(value) => console.log(value)}
              items={ketPicker}
              Icon={
                () => <Icon name="arrow-drop-down" />
              }
            />
          </View>
        </View>
      </View>
    )
  }
}

export default FormHasilUkur
