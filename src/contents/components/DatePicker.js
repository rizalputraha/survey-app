import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  Text,
} from 'react-native';
import { Button, Icon } from 'react-native-elements';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';

export default class DatePicker extends Component {
  constructor(props) {
    super(props)
    this.state = {
      date: new Date(),
      text: null,
      show: false,
      minimumDate: new Date()
    }
  }

  componentDidMount() {

    this.setState({
      date: new Date(),
      text: this.props.getDate == '' ? null : this.props.getDate
    })
  }

  _onDateTimePress = () => {
    this.setState({
      show: !this.state.show
    })
  }

  _setdate(date) {
    var newFormatDate = moment(date).format('DD/MM/YYYY');
    if (date == undefined)
      date = new Date();
    this.setState({
      date: date,
      text: newFormatDate,
      show: !this.state.show
    });
    this.props.setDate(newFormatDate);
    console.log(this.state, 'log setDate component');

    return newFormatDate;
  }

  _onDateTimPicked = (event, date) => {
    this._setdate(date);
  }

  _getValidationRequire() {
    const { text } = this.state
    if (text == null) {
      return <Icon name='date-range' containerStyle={{ marginLeft: 10 }} type='material' color='white' />
    }
    return <Icon name='done' containerStyle={{ marginLeft: 10 }} type='material' color='#b2ff59' />;
  }

  renderButtonDate() {
    const { show, date, text } = this.state

    return (
      <View style={{ paddingHorizontal: 15 }}>
        <Text style={styles.label}>{this.props.label}</Text>
        <Button
          type={'outline'}
          icon={this._getValidationRequire()}
          iconRight
          title={text !== null ? text : 'Pilih Tanggal'}
          onPress={() => this._onDateTimePress()}
          buttonStyle={{ backgroundColor: '#304ffe' }}
          titleStyle={{ color: 'white', fontSize: 12 }}
        />
        {
          show &&
          <RNDateTimePicker
            display="default"
            onChange={(event, date) => this._onDateTimPicked(event, date)}
            value={date}
            mode={'date'}
            minimumDate={this.state.minimumDate}
          />
        }
      </View>
    )
  }

  render() {
    return (
      <View>
        {this.renderButtonDate()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  label: {
    fontSize: 14,
    paddingVertical: 10,
    color: '#b4b4b4',
    fontFamily: 'DroidSans',
  },
  datePickerBox: {
    paddingTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FAFAFA'
  },
  datePickerText: {
    fontSize: 14,
    color: 'black',
  },
});
