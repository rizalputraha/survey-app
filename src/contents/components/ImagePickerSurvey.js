import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'
import ImagePicker from 'react-native-image-picker'
import { Button, Icon } from 'react-native-elements'

export class ImagePickerSurvey extends Component {
    constructor(props) {
        super(props);
        this.state = {
            options: {
                maxWidth: 800,
                maxHeight: 600,
                title: 'Select Avatar',
                customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
                storageOptions: {
                    skipBackup: true,
                    path: 'images',
                },
            }
        }
    }


    selectCamera() {
        ImagePicker.launchCamera(this.state.options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                this.props.onSelectFiles(source)
            }
        });
    }

    selectGallery() {
        ImagePicker.launchImageLibrary(this.state.options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                this.props.onSelectFiles(source)
            }
        });
    }

    render() {
        return (
            <View>
                <Button
                    containerStyle={{ padding: 10 }}
                    titleStyle={{ marginRight: 10 }}
                    icon={<Icon name="camera-alt" color='white' />}
                    iconRight
                    title="Select From Camera"
                    onPress={() => this.selectCamera()} />
                <Button
                    containerStyle={{ padding: 10 }}
                    titleStyle={{ marginRight: 10 }}
                    icon={<Icon name="photo" color='white' />}
                    iconRight
                    title="Select From Gallery"
                    onPress={() => this.selectGallery()} />
            </View>
        )
    }
}

export default ImagePickerSurvey
