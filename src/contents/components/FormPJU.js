import React, { Component } from 'react'
import { Text, View } from 'react-native'
import InputComponent from "./InputComponent";
import RNPickerSelect from 'react-native-picker-select'
import { ketPicker } from '../../constants/picker'
import { Icon } from 'react-native-elements';

export class FormPJU extends Component {
    render() {
        return (
            <View style={{ flex: 1, marginVertical: 10 }}>
                <Text style={{ fontSize: 32, fontWeight: 'bold', marginVertical: 10 }}>Sisi PJU</Text>
                <View style={{ backgroundColor: '#BCE0FD' }}>
                    <View style={{ paddingHorizontal: 10 }}>
                        <InputComponent
                            placeholder="Jumlah MCB"
                            placeholderTextColor="#2699FB" />
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                        <Text style={{ flex: 1 }}>MCB</Text>
                        <RNPickerSelect
                            style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                            onValueChange={(value) => console.log(value)}
                            items={ketPicker}
                            Icon={
                                () => <Icon name="arrow-drop-down" />
                            }
                        />
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                        <Text style={{ flex: 1 }}>Kontraktor</Text>
                        <RNPickerSelect
                            style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                            onValueChange={(value) => console.log(value)}
                            items={ketPicker}
                            Icon={
                                () => <Icon name="arrow-drop-down" />
                            }
                        />
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                        <Text style={{ flex: 1 }}>Switch</Text>
                        <RNPickerSelect
                            style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                            onValueChange={(value) => console.log(value)}
                            items={ketPicker}
                            Icon={
                                () => <Icon name="arrow-drop-down" />
                            }
                        />
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                        <Text style={{ flex: 1 }}>Grounding</Text>
                        <RNPickerSelect
                            style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                            onValueChange={(value) => console.log(value)}
                            items={ketPicker}
                            Icon={
                                () => <Icon name="arrow-drop-down" />
                            }
                        />
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10 }}>
                        <Text style={{ flex: 1 }}>Kabel Outer</Text>
                        <RNPickerSelect
                            style={{ iconContainer: { top: 10, right: 10 }, viewContainer: { flex: 1 }, inputAndroid: { color: '#000', backgroundColor: 'white', marginHorizontal: 10 } }}
                            onValueChange={(value) => console.log(value)}
                            items={ketPicker}
                            Icon={
                                () => <Icon name="arrow-drop-down" />
                            }
                        />
                    </View>
                </View>
            </View>
        )
    }
}

export default FormPJU
