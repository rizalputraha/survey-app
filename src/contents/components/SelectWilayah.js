import React, { Component } from 'react'
import { Text, View, Modal, FlatList, TouchableOpacity } from 'react-native'
import { Button, Icon, Input } from "react-native-elements";
import HeaderComponent from './HeaderComponent';

export class SelectWilayah extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isModalVisible: false,
            searchKey: '',
            data: this.props.data
        }

    }

    setValue(item) {
        this.setState({
            selected: item,
            isModalVisible: false,
        });
        this.props.onChange(item);
    }

    _setModalVisible(state) {
        this.setState({ isModalVisible: state })
    }

    SearchFilterFunction(text) {
        const newData = this.state.data.filter(function (item) {
            const itemData = item.toUpperCase()
            const textData = text.toUpperCase()
            return itemData.indexOf(textData) > -1
        })
        this.setState({ data: newData })
    }

    _searchData(value) {
        this.setState({ searchKey: value })
        if (this.state.searchKey.length > 1) {
            this.SearchFilterFunction(value);
        } else {
            this.setState({ data: this.props.data })
        }
    }

    renderItem(item) {
        return (
            <TouchableOpacity onPress={() => this.setValue(item)} style={{ padding: 20, margin: 10, borderRadius: 5, backgroundColor: 'white', elevation: 3 }}>
                <Text>{item}</Text>
            </TouchableOpacity>
        )
    }

    renderModal() {
        return (
            <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.isModalVisible}
                onRequestClose={() => { this._setModalVisible(!this.state.isModalVisible); }}>
                <View>
                    <HeaderComponent
                        title={this.props.titleEmptyData}
                        pushBackButtonHeader={() => this._setModalVisible(!this.state.isModalVisible)}
                    />
                    <View style={{ padding: 15 }}>
                        <Input
                            containerStyle={{ marginBottom: 10, paddingHorizontal: 0 }}
                            onChangeText={(value) => this._searchData(value)}
                            placeholder={this.props.placeholder}
                            value={this.state.searchKey}
                            rightIcon={
                                <Icon
                                    type="material"
                                    name="search"
                                    size={24}
                                />
                            }
                        />
                        <FlatList
                            data={this.state.data}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item }) => this.renderItem(item)}
                        />
                    </View>
                </View>
            </Modal>
        )
    }

    renderButtonModal() {
        return (
            <Button
                onPress={() => this._setModalVisible(!this.state.isModalVisible)}
                titleStyle={{ color: '#2699FB' }}
                buttonStyle={{ borderWidth: 1, borderColor: '#BCE0FD', backgroundColor: 'white', padding: 15, justifyContent: 'space-between' }}
                title={this.state.selected != null ? this.state.selected : 'Pilih Wilayah'}
                iconRight
                icon={
                    <Icon
                        name="arrow-drop-down"
                        size={24}
                        color="#2699FB"
                    />
                }
            />
        )
    }

    render() {
        return (
            <View style={{ marginTop: 10 }}>
                {this.renderModal()}
                {this.renderButtonModal()}
            </View>
        )
    }
}

export default SelectWilayah
